﻿using Collection.Collections;
using Collection.Collections.ObservableCollection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Collection
{
    class ControllClass
    {
        public static void Main()
        {
            Console.WriteLine("ArrayList");
            ArrayListCollection arrayList = new ArrayListCollection();
            arrayList.ArrayListLesson();

            Console.WriteLine("\n\n\n");
            Console.WriteLine("List<T>");
            ListTCollection listT = new ListTCollection();
            listT.ListTLesson();

            Console.WriteLine("\n\n\n");
            Console.WriteLine("LinkedList<T>");
            LinkedListTCollection linkedListT = new LinkedListTCollection();
            linkedListT.ListListT();

            Console.WriteLine("\n\n\n");
            Console.WriteLine("Queue");
            QueueTCollection queueT = new QueueTCollection();
            queueT.QueueT();

            Console.WriteLine("\n\n\n");
            Console.WriteLine("Stack<T>");
            StackTCollection stackT = new StackTCollection();
            stackT.StackT();

            Console.WriteLine("\n\n\n");
            Console.WriteLine("Dictionary<T,V>");
            DictionryTVCollection dictionryTV = new DictionryTVCollection();
            dictionryTV.DictionaryTV();

            Console.WriteLine("\n\n\n");
            Console.WriteLine("ObservableCollection");
            ObservableCollectionExample OCollerction = new ObservableCollectionExample();
            OCollerction.OCollection();

        }
    }
}
