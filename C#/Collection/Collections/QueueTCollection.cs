﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Collection.Collections
{
    public class QueueTCollection
    {
        public void QueueT()
        {
            Queue<string> fRecords = new Queue<string>();
            Queue<string> sRecords = new Queue<string>();

            fRecords.Enqueue("First rec");
            fRecords.Enqueue("Second rec");
            fRecords.Enqueue("Third rec");

            Console.WriteLine("\nFirst Queue\n");
            foreach(var rec in fRecords)
            {
                Console.WriteLine(rec);
            }

            fRecords.Enqueue("200");
            fRecords.Enqueue("300");

            sRecords.Enqueue(fRecords.Dequeue());
            sRecords.Enqueue(fRecords.Dequeue());
            sRecords.Enqueue(fRecords.Dequeue());
            sRecords.Enqueue(fRecords.Peek());

            Console.WriteLine("\nFirst Queue\n");
            foreach (var rec in fRecords)
            {
                Console.WriteLine(rec);
            }
            Console.WriteLine("\nSecond Queue\n");
            foreach (var rec in sRecords)
            {
                Console.WriteLine(rec);
            }




        }
    }
}
