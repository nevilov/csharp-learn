﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Collection.Collections
{
    public class LinkedListTCollection
    {
        public void ListListT()
        {
            LinkedList<int> list = new LinkedList<int>();
            list.AddLast(55);
            list.AddLast(54);
            list.AddLast(53);
            list.AddFirst(1);
            LinkedListNode<int> findNode = list.Find(2);
            if (findNode != null)
                list.AddAfter(findNode, 33);

            foreach(var rec in list)
            {
                Console.WriteLine(rec);
            }

        }
        
        
           
    }
}
