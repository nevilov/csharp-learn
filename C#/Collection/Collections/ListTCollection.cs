﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Collection
{
    class ListTCollection
    {
        public void ListTLesson() {
            List<string> tList = new List<string>();

            tList.Add("Hello");
            tList.Add("Its me");
            tList.Add("Some simple");
            tList.Add("Text");
            tList.Add("4");
            tList.Add("7");
            tList.Add("2");

            foreach(var list in tList)
            {
                Console.WriteLine(list);
            }

            tList.RemoveAt(0);
            tList.Insert(3, "New Text");
            

            tList.Sort();
            Console.WriteLine("AfterSort");
            foreach (var list in tList)
            {
                Console.WriteLine(list);
            }                
        }
    }
}
