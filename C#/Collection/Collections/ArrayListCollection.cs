﻿using System;
using System.Collections;

namespace Collection
{
    class ArrayListCollection
    {
        public void ArrayListLesson()
        {
            ArrayList mList = new ArrayList();
            mList.Add(2345);
            mList.Add(false);
            mList.Add("sample text");
            mList.AddRange(new int[] { 4, 2, 3, 5, 1 }); //Some objects
            
            foreach(var rec in mList)
            {
                Console.WriteLine(rec);
            }

            Console.WriteLine("Delete 3 elements");
            mList.RemoveAt(0);
            mList.RemoveAt(0);
            mList.RemoveAt(0);

            foreach (var rec in mList)
            {
                Console.WriteLine(rec);
            }

            Console.WriteLine("Reverse list");
            mList.Reverse();
            foreach (var rec in mList)
            {
                Console.WriteLine(rec);
            }


        }
    }
}
