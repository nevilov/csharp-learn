﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Collection.Collections
{
    public class StackTCollection
    {
        public Random rnd = new Random();
        public void StackT()
        {
            Stack<int> myStack = new Stack<int>();

            for(int i = 0; i < 10; ++i) 
            {
                myStack.Push(rnd.Next(100));
            }

            Console.WriteLine("\n Stack \n");
            foreach(var rec in myStack)
            {
                Console.Write(rec + "\t");
            }

            myStack.Pop();
            myStack.Pop();
            myStack.Pop();

            Console.WriteLine("\n Stack without 3 first elements \n");
            foreach (var rec in myStack)
            {
                Console.Write(rec + "\t");
            }



        }
    }
}
