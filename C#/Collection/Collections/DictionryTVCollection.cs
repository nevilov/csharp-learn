﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Collection.Collections
{
    class DictionryTVCollection
    {
        public void DictionaryTV()
        {
            Dictionary<int, string> dictLagnuage = new Dictionary<int, string>();
            dictLagnuage.Add(1, "Русский");
            dictLagnuage.Add(2, "Турецкий");
            dictLagnuage.Add(3, "Английский");
            dictLagnuage.Add(255, "Хинди");

            foreach(var d in dictLagnuage)
            {
                Console.WriteLine(d);
            }

            foreach (KeyValuePair<int, string> keyValue in dictLagnuage)
            {
                Console.WriteLine(keyValue.Key + " - " + keyValue.Value);
            }

            Dictionary<string, string> dictCountries = new Dictionary<string, string>
            {
                ["Германия"] = "Берлин",
                ["Россия"] = "Москва"
            };

            foreach (KeyValuePair<string, string> keyValue in dictCountries)
            {
                Console.WriteLine(keyValue.Key + " - " + keyValue.Value);
            }

        }
    }
}
