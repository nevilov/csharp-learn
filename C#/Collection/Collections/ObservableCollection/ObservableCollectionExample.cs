﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;

namespace Collection.Collections.ObservableCollection
{
    public class ObservableCollectionExample
    {
        public void OCollection()
        {
            ObservableCollection<string> ex = new ObservableCollection<string>();
            ex.Add("ExampleText ");

            ex.CollectionChanged += NotifyCollection;

            ex.Add("ExampleText 2");
            ex.Add("ExampleText 3");

        }

        public void NotifyCollection(object sender, NotifyCollectionChangedEventArgs e)
        {
            string Text = e.NewItems[0] as string;
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    Console.WriteLine($"Добавлен новый объект: {Text}");
                    break;

                case NotifyCollectionChangedAction.Remove:
                    Console.WriteLine($"Удален объект {Text}");
                    break;
            }
        }
        
    }
}
