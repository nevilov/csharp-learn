using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Basics
{
    public class Startup
    {
       
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            int x = 1;

            app.Run(async (p) =>
            {
                x *= 5;
                await p.Response.WriteAsync($"{x}");
            });

            // ���� ���������� � �������� ����������
            if (env.IsDevelopment())
            {
                // �� ������� ���������� �� ������, ��� ������� ������
                app.UseDeveloperExceptionPage();
            }
            // ��������� ����������� �������������
            app.UseRouting();

            // ������������� ������, ������� ����� ��������������
            app.UseEndpoints(endpoints =>
            {
                // ��������� ������� - �������� ��������� ������� � ���� ������� context
                endpoints.MapGet("/", async context =>
                {
                    // �������� ������ � ���� ������ "Hello World!"
                    await context.Response.WriteAsync("Hello World!");
                });
            });
        }
    }
}
